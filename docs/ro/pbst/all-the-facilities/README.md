# Toate facilitățile

:::danger Please note: This part of the handbook **may** get some changes to their descriptions. Like locations for uniform givers. :::

## Pinewood Computer Core (PBCC)
Explore a secret underground facility that's about to explode! Your mission is to keep a supercomputer from overheating. Explore every corner of the facility, survive disasters, press every button, discover secrets, and you may just make it out alive!

📙 This game was featured in the Roblox Top Adventure Games book, so check it out!

- Apasă pe iconița de mai jos pentru a te alătura Pinewood Builders pentru beneficii suplimentare!

- Ajutoare + credite: Irreflexive, LENEMAR și în special Csdi. SigmaTech, Terrariabat, TheGreatOmni, Unsayableorc pentru ajutor cu armele și obiectele. Mulțumim Jay Cosmic pentru permisiunea de a folosi melodia ta.

## Pinewood Space Shuttle Advantage (PBSSA)
Launch and ride the Space Shuttle Advantage into space! Then complete your satellite deployment mission into orbit and return to Earth with your team, reach the Space Station, the Moon and other planets, and then bravely rocket up to the alien home world to discover extra-terrestrial weapons!

Project Advantage, our effort to launch a network of interconnected laser satellites! This game was featured in the Roblox Spotlight blog and the 2013 London Bloxcon. Released to public- 05/28/2012

- Apasă pe iconița de mai jos pentru a te alătura Pinewood Builders pentru beneficii suplimentare!

- Ajutoare + Credite: RedDwarfIV, Irreflexive, LENEMAR și Csdi pentru ajutor cu armele.

## Pinewood Research Facility (PBRF)
Welcome to Pinewood Research Facility, located in a secret part of the Sahara Desert and above the underground Pinewood Computer Core it serves as the hub between our main facilities and games making up the Sahara complex - State of the art mega-structures and shuttle launch sites, as well as the home of our supercomputer mainframe and satellite communications for our space projects.

You can come and visit the exhibition, where we show you our past technology and creations. This place also features the Pinewood astronaut training centrifuge capable of incredibly high G's, so buckle up.

This facility was first proposed in 2010, after decommissioning the original 2008 Pinewood Labs, the 2009 Research Facility, and the facilities at the 2009 HQ. Any areas still under construction will be blocked off with a white wall.

## Pinewood Headquarters (PBHQ)
Welcome to our group headquarters. First built in 2011, this has been the main meeting place and headquarters for Pinewood Builders, located in Tokyo, Japan; we welcome all visitors.

This game was featured in the Roblox Blog 2013, in "[Six ROBLOX Places That'll Make You Say "Whoa"](https://blog.roblox.com/2013/06/six-roblox-places-thatll-make-you-say-whoa/)".

## Pinewood Builders Data Storage Facility (PBDSF)
Located in Wyoming, this dedicated data center stores points and other information for divisions of Pinewood Builders.

Irreflexive - Database Scripter Csdi - Main Builder / UI Design spyagent388 - Assistant Builder SADENNING - Minor Building, Ideas

## PBST Training Facility (PBSTTF)
Project CRATER 2015, otherwise known as the official PBST Training Facility is located just outside of Pinewood Research Facility in the great Sahara. The impact crater plays host to a wide variety of training activities and scenarios, as well as gladiator arenas to separate the skilled.

The climate is maintained thanks to a huge air conditioned bio-dome above the crater and access is via a state of the art monorail system. The PBST Shooting dome is also located here.

## PBST Activity Center (PBSTAC)
Brand new training, self-training, and patrol zone! This facility contains five times the activities used in the former "Project CRATER", and many activities in the simulation room. Join PBST to gain access to the facility, and to attend awesome trainings!

Officially released to the public as of June 12th 2018.

## PBST Hub (PBSTH)
This hub serves as a teleportation place to all PBST games, patrol facilities, and other games! It also serves as a central information place for new members to learn about PBST!
